#!/home/dustin/bin/py

from skein import skein1024
import random, string
import requests

a="0"+bin(int('5b4da95f5fa08280fc9879df44f418c8f9f12ba424b7757de02bbdfbae0d4c4fdf9317c80cc5fe04c6429073466cf29706b8c25999ddd2f6540d4475cc977b87f4757be023f19b8f4035d7722886b78869826de916a79cf9c94cc79cd4347d24b567aa3e2390a573a373a48a5e676640c79cc70197e1c5e7f902fb53ca1858b6',16))[2:]

def rando():
    c = string.digits+string.ascii_letters
    return ''.join(random.choice(c) for i in range(random.randint(0,1024)))

def diff(s):
    h = skein1024(bytes(s,"utf8"))
    b = bin(int(h.hexdigest(),16))[2:]
    d = 0
    while len(b) < 1024 :
        b = "0" + b
    for i in range(len(a)):
        if a[i] is not b[i]:
            d += 1
    return d

_min_d = 1000
_min_s = ''
while True:
    s = rando()
    d = diff(s)
    if d < _min_d :
        _min_d = d
        _min_s = s
        print("********************* %d ******************" % (d))
        print("%s" % (s))
        payload = {"hashable":s}
        r=requests.post('http://almamater.xkcd.com/?edu=drexel.edu',data=payload)
        print(r.text.split(' ')[9])
